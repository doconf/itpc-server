var request = require('request');

module.exports = function callApi(url, cb) {
    const API_URL = 'https://blockchain.info/';
    var options = {
        url: API_URL + url,
        // port: 9000,
        method: "GET",
        headers: {
            'User-Agent': 'request',
            'cors': 'true',
            'access-control-allow-origin': '*',
            'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, Authorization, cors, Access-Control-Allow-Origin'
        }
    };
    function callback(error, response, body,) {
        // console.log(" Response -> ", body);
        if (!error && response.statusCode === 200) {
            cb && cb(JSON.parse(body))
        }
    }
        
    request(options, callback);
}