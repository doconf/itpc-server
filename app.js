var helmet = require('helmet');
var bodyParser = require('body-parser');
var compression = require('compression');
var methodOverride = require('method-override');
var cors = require('cors');
var express = require('express');
var callApi = require('./utils')

var app = express();

app.use(function(req, res, next) {
//   res.header("Access-Control-Allow-Origin", 'http://locahost:8000');
    res.header("Access-Control-Allow-Origin", process.env.CLIENT_HOST || 'http://locahost:8000' );
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, X-Access-Token");
    next();
});    

app.use(bodyParser.json({ limit: '5mb' }));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(methodOverride());  //Lets you use HTTP verbs such as PUT or DELETE in places where the client doesn't support it.
app.use(compression()); //The middleware will attempt to compress response bodies for all request 
app.use(helmet()); //Helmet helps you secure your Express apps by setting various HTTP headers

app.use(cors({ origin: true, credentials: true }));  // Has to be more detailed;; NEEDS MORE INPUTS

// This responds with "Hello World" on the homepage
app.get('/', function (req, res) {
   console.log("Got a GET request for the homepage");
   res.send('Hello GET');
})

// This responds a GET request for the /list_user page.
app.get('/latestblock', function (req, res) {
    console.log("Got a GET request for /latest-block");
    callApi('latestblock', function(result) {
        res.json(result);
    })
})

// This responds a GET request for abcd, abxcd, ab123cd, and so on
app.get('/rawblock/:id', function(req, res) {   
   console.log("Got a GET request for /single-block", req.params.id);
   callApi('rawblock/'+ req.params.id, function(result) {
       res.json(result);
   })
})

// This responds a GET request for abcd, abxcd, ab123cd, and so on
app.get('/rawtx/:id', function(req, res) {   
   console.log("Got a GET request for /single-transaction", req.params.id);
   callApi('rawtx/'+ req.params.id, function(result) {
       res.json(result);
   })
})

var server = app.listen(process.env.PORT || 4000, function () {
   var host = 'https://code-challenge-server-itpc.herokuapp.com'
   var port = 4000
   
   console.log("ITPC server app listening at http://%s:%s", host, port)
})